from alpine
workdir /src

run apk update && apk upgrade --no-cache && apk add --no-cache gcc g++ binutils make nodejs yarn vips-dev

copy package.json yarn.lock .yarnrc.yml ./
copy .yarn .yarn
run yarn

copy . .

run yarn build --no-autoinstall

cmd yarn start
