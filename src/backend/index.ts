import 'reflect-metadata'
import path = require('path')
import fs = require('fs')
import fastify from 'fastify'
import fastifyStatic from 'fastify-static'
import {createConnection} from 'typeorm'
import {Comment, CommentPOD} from '../common/Comment'
import {User, UserPOD} from '../common/User'
import {Event, EventPOD} from '../common/Event'
import {PostgresConnectionOptions} from 'typeorm/driver/postgres/PostgresConnectionOptions'
import {SqliteConnectionOptions} from 'typeorm/driver/sqlite/SqliteConnectionOptions'
import fastifyCookie from 'fastify-cookie'
import {Static, Type} from '@sinclair/typebox'

const isPostgres = process.env.DATABASE_TYPE === 'postgres'

const postgresOptions: PostgresConnectionOptions = {
  type: 'postgres',
  url: process.env.DATABASE_URL,
  ssl: {
    rejectUnauthorized: false,
  },
}

const sqliteOptions: SqliteConnectionOptions = {
  type: 'sqlite',
  database: 'test.db',
}

async function main() {
  const db = await createConnection({
    ...(isPostgres ? postgresOptions : sqliteOptions),
    entities: [User, Comment, Event],
    synchronize: true,
  })

  const server = fastify()

  server.register(fastifyCookie)

  server.decorateRequest('user', undefined)
  server.addHook('onRequest', async req => {
    try {
      const {token} = req.cookies
      if (token === undefined) {
        return
      }
      const [userStr, password] = token.split(':')
      const userId = parseInt(userStr)
      const user = await db.manager.findOneOrFail(User, userId)
      if (user.password != password) {
        throw 'Invalid password'
      }
      ;(req as any).user = user
    } catch (reason) {
      throw {
        statusCode: 403,
        error: 'Unauthorized',
        message: reason,
      }
    }
  })

  const LoginPOD = Type.Object({
    name: Type.String(),
    password: Type.String(),
  })

  server.post<{Body: Static<typeof LoginPOD>}>(
    '/api/login',
    {schema: {body: LoginPOD}},
    async ({body: {name, password}}, res) => {
      const user = await db.manager.findOneOrFail(User, {name})
      if (user.password !== password) {
        throw {
          statusCode: 403,
          error: 'Unauthorized',
          message: 'Invalid password',
        }
      }
      res.cookie('token', `${user.id}:${user.password}`, {
        expires: new Date(Date.now() + 1000 * 60 * 60 * 24 * 5), // 5 days
        httpOnly: true,
        sameSite: true,
        secure: process.env.NODE_ENV === 'production',
      })
      return {value: 'ok'}
    }
  )

  server.get('/api/self', async ({user}: any) => ({
    value: user,
  }))

  server.get('/api/self/comments', async ({user}: any) => ({
    value: await user.comments,
  }))

  server.get('/api/self/events', async ({user}: any) => ({
    value: await user.events,
  }))

  server.post<{Body: Static<typeof UserPOD>}>(
    '/api/users',
    {schema: {body: UserPOD}},
    async ({body: user}, res) => {
      await db.manager.insert(User, user)
      res.cookie('token', `${user.id}:${user.password}`, {
        expires: new Date(Date.now() + 1000 * 60 * 60 * 24 * 5), // 5 days
        httpOnly: true,
        sameSite: true,
        secure: process.env.NODE_ENV === 'production',
      })
      res.send({value: user})
    }
  )

  const ParamsPOD = Type.Object({
    id: Type.Number(),
  })

  server.put<{Body: Static<typeof UserPOD>; Params: Static<typeof ParamsPOD>}>(
    '/api/users/:id',
    {schema: {body: UserPOD, params: ParamsPOD}},
    async ({body: user, params: {id}}) => {
      user.id = id
      await db.manager.save(User, user)
      return {value: user}
    }
  )

  server.get<{Params: Static<typeof ParamsPOD>}>(
    '/api/users/:id',
    {schema: {params: ParamsPOD}},
    async ({params: {id}}) => {
      const {name, email} = await db.manager.findOneOrFail(User, id)
      return {value: {name, email, id}}
    }
  )

  server.get<{Params: Static<typeof ParamsPOD>}>(
    '/api/users/:id/comments',
    {schema: {params: ParamsPOD}},
    async ({params: {id}}) => {
      const user = await db.manager.findOneOrFail(User, id)
      return {value: await user.comments}
    }
  )

  server.get<{Params: Static<typeof ParamsPOD>}>(
    '/api/users/:id/events',
    {schema: {params: ParamsPOD}},
    async ({params: {id}}) => {
      const user = await db.manager.findOneOrFail(User, id)
      return {value: await user.events}
    }
  )

  server.get('/api/comments', async _ =>
    Promise.all(
      (await db.manager.find(Comment)).map(async ({text, author, event}) => ({
        text,
        author: (await author)?.id,
        event: (await event)?.id,
      }))
    )
  )

  server.post<{Body: Static<typeof CommentPOD>}>(
    '/api/comments',
    {schema: {body: CommentPOD}},
    async req => {
      const {eventId, text} = req.body
      const comment = db.manager.create(Comment, {text})
      comment.author = (req as any).user
      comment.event = Promise.resolve(
        await db.manager.findOneOrFail(Event, eventId)
      )
      await db.manager.save(Comment, comment)
      return {value: comment}
    }
  )

  server.post<{Body: Static<typeof EventPOD>}>(
    '/api/events',
    {schema: {body: EventPOD}},
    async req => {
      if ((req as any).user === undefined) {
        throw {statusCode: 403, error: 'Unauthorized', message: 'Login first'}
      }
      const event = db.manager.create(Event, {
        ...req.body,
        author: (req as any).user,
        likes: 0,
      })
      event.author = Promise.resolve(
        await db.manager.findOneOrFail(User, (req as any).user.id)
      )
      await db.manager.save(Event, event)
      return {value: event}
    }
  )

  server.get('/api/events', async _ => ({
    value: await db.manager.find(Event),
  }))

  server.get<{Params: Static<typeof ParamsPOD>}>(
    '/api/events/:id',
    {schema: {params: ParamsPOD}},
    async ({params: {id}}) => ({
      value: await db.manager.findOneOrFail(Event, id),
    })
  )

  server.get<{Params: Static<typeof ParamsPOD>}>(
    '/api/events/:id/comments',
    {schema: {params: ParamsPOD}},
    async ({params: {id}}) => {
      const event = await db.manager.findOneOrFail(Event, id)
      return {
        value: await Promise.all(
          (await event.comments)!.map(async ({id, text, author}) => ({
            id,
            text,
            authorId: (await author!).id,
          }))
        ),
      }
    }
  )

  server.post<{Params: Static<typeof ParamsPOD>}>(
    '/api/events/:id/like',
    {schema: {params: {ParamsPOD}}},
    async ({params: {id}}) => {
      const event = await db.manager.findOneOrFail(Event, id)
      event.likes! += 1
      await db.manager.save(Event, event)
      return {value: event}
    }
  )

  server.post<{Params: Static<typeof ParamsPOD>}>(
    '/api/events/:id/dislike',
    {schema: {params: {ParamsPOD}}},
    async ({params: {id}}) => {
      const event = await db.manager.findOneOrFail(Event, id)
      event.likes! -= 1
      await db.manager.save(Event, event)
      return {value: event}
    }
  )

  const staticDir = path.resolve(__dirname, '..', '..', 'dist')

  server.register(fastifyStatic, {
    root: staticDir,
    prefix: '/app',
    wildcard: false,
  })

  server.get('/app/*', (_req, res) => {
    res.type('text/html')
    res.send(fs.createReadStream(path.resolve(staticDir, 'index.html')))
  })

  server.get('/', async (_req, res) => {
    res.redirect('/app/')
  })

  const address = await server.listen(process.env.PORT || 8080, '0.0.0.0')

  console.log(`Listening on ${address}`)
}

main()
