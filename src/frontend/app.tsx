import {h, render} from 'preact'
import App from './pages/App'

render(<App />, document.body)
