import {h, Fragment, FunctionComponent} from 'preact'
import {Link} from 'preact-router'
import {Button, ButtonColor, ButtonSize, ButtonStyle} from './Button'

import '../styles/HeroSection.scss'

export interface Props {
  lightBg: boolean
  topLine: string
  lightText: boolean
  lightTextDesc: boolean
  headLine: string
  description: string
  buttonLabel: string
  isLeftSided?: boolean
  src: string
  alt: string
}

export const HeroSection: FunctionComponent<Props> = ({
  lightBg,
  topLine,
  lightText,
  lightTextDesc,
  headLine,
  description,
  buttonLabel,
  isLeftSided = false,
  src,
  alt,
}) => {
  return (
    <>
      <div
        className={lightBg ? 'home__hero-section' : 'home__hero-section darkBg'}
      >
        <div className='container'>
          <div
            className='row home__hero-row'
            style={{
              display: 'flex',
              flexDirection: isLeftSided === true ? 'row-reverse' : 'row',
            }}
          >
            <div className='col'>
              <div className='home__hero-text-wrapper'>
                <div className='top-line'>{topLine}</div>
                <h1 className={lightText ? 'heading' : 'heading dark'}>
                  {headLine}
                </h1>
                <p
                  className={
                    lightTextDesc
                      ? 'home__hero-subtitle'
                      : 'home__hero-subtitle dark'
                  }
                >
                  {description}
                </p>
                <Link href='/app/signup'>
                  <Button
                    buttonStyle={ButtonStyle.Primary}
                    buttonSize={ButtonSize.Wide}
                    buttonColor={ButtonColor.Blue}
                  >
                    {buttonLabel}
                  </Button>
                </Link>
              </div>
            </div>
            <div className='col'>
              <div className='home__hero-img-wrapper'>
                <img src={src} alt={alt} className='home__hero-img' />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
