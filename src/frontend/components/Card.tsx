import {h} from 'preact'
import {Link} from 'preact-router'
import {
  Button,
  ButtonColor,
  ButtonSize,
  ButtonStyle,
} from '../components/Button'
import Icon from 'preact-material-components/Icon'

import '../styles/Card.scss'

export default () => {
  return (
    <div className='card__section'>
      <div className='card__wrapper'>
        <h1 className='card__heading'>Features</h1>
        <div className='card__container'>
          <Link href='/app/signup' className='card__container-card'>
            <div className='card__container-cardInfo'>
              <div className='icon'>
                <Icon icon={['code', '2x']} />
              </div>
              <h3>Share opinions</h3>
              <h4>Comment</h4>
              <p>like and dislike</p>
              <ul className='card__container-features'>
                <li>rate your friend's party</li>
                <li>see comments below your event</li>
                <li>build new communications</li>
              </ul>
              <Button
                buttonStyle={ButtonStyle.Outline}
                buttonSize={ButtonSize.Wide}
                buttonColor={ButtonColor.Primary}
              >
                Go ahead
              </Button>
            </div>
          </Link>
          <Link href='/app/signup' className='card__container-card'>
            <div className='card__container-cardInfo'>
              <div className='icon'>
                <Icon>school</Icon>
              </div>
              <h3>Universities</h3>
              <h4>Is your uni best?</h4>
              <ul className='card__container-features'>
                <li>see local events</li>
                <li>bring new people</li>
                <li>compete with other universities</li>
              </ul>
              <Button
                buttonStyle={ButtonStyle.Outline}
                buttonSize={ButtonSize.Wide}
                buttonColor={ButtonColor.Blue}
              >
                Register
              </Button>
            </div>
          </Link>
          <Link href='/app/signup' className='card__container-card'>
            <div className='card__container-cardInfo'>
              <div className='icon'>
                <Icon icon={['code', '2x']} />
              </div>
              <h3>Top</h3>
              <h4>10</h4>
              <p>between everyone</p>
              <ul className='card__container-features'>
                <li>Earn reputation</li>
                <li>Become leader</li>
                <li>Host the best celebrations</li>
              </ul>
              <Button
                buttonStyle={ButtonStyle.Outline}
                buttonSize={ButtonSize.Wide}
                buttonColor={ButtonColor.Primary}
              >
                Jump in
              </Button>
            </div>
          </Link>
        </div>
      </div>
    </div>
  )
}
