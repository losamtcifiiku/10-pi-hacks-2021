import {h, FunctionComponent} from 'preact'

import Card from 'preact-material-components/Card'
import 'preact-material-components/Card/style.css'

type Props = {
  author: string
  text: string
}

export const Comment: FunctionComponent<Props> = ({author, text}) => (
  <Card className='comment'>
    <div class='card-header'>
      <h3 class='mdc-typography--title'>{author}</h3>
      <div class='mdc-typography--caption'>{text}</div>
    </div>
  </Card>
)
