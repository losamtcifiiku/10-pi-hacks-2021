import {Static} from '@sinclair/typebox'
import {h, Fragment, FunctionComponent} from 'preact'
import {EventPOD} from '../../common/Event'

import Card from 'preact-material-components/Card'
import 'preact-material-components/Card/style.css'

import {PostComment} from '../components/PostComment'
import Comments from './Comments'

import '../styles/Event.scss'

type Props = {
  event: Static<typeof EventPOD>
  full?: boolean
  onLike?: () => any
  onDislike?: () => any
}

export const Event: FunctionComponent<Props> = ({
  event: {id, title, date, place, university, description, likes, author},
  full,
  onLike,
  onDislike,
}) => (
  <>
    <Card className='card'>
      <div class='card-header'>
        <div class='card-title'>{title}</div>
        <h4 class='card-date'>{'(date: ' + date + ')'}</h4>
      </div>
      <div class='card-geo'>
        <h2 class='card-place'>{'[Place:] ' + place}</h2>
        <h2 class='card-university'>{'{Uni:} ' + university}</h2>
      </div>
      <p class='card-description'>{description}</p>
      <Card.Media className='card-media' />
      <Card.Actions>
        <Card.ActionIcons>
          {full && <Card.ActionIcon onclick={onLike}>thumb_up</Card.ActionIcon>}
          <span>
            {!full && 'Likes: '}
            {likes}
          </span>
          {full && (
            <Card.ActionIcon onclick={onDislike}>thumb_down</Card.ActionIcon>
          )}
        </Card.ActionIcons>
      </Card.Actions>
    </Card>
    {full && <Comments id={id!} />}
    {full && <PostComment id={id!} />}
  </>
)

export default Event
