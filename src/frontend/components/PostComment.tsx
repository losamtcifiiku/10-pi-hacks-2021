import {h, Fragment, FunctionComponent} from 'preact'
import TextField from 'preact-material-components/esm/TextField'
import 'preact-material-components/TextField/style.css'
import {useState} from 'preact/hooks'
import {postComment} from '../../common/Api'
import {Button} from './Button'

import '../styles/Form.scss'

type Props = {
  id: number
}

export const PostComment: FunctionComponent<Props> = ({id}) => {
  const [text, setText] = useState('')
  const [status, setStatus] = useState('')

  async function submit(e: Event) {
    e.preventDefault()
    try {
      await postComment(id, text)
      setStatus('Successfully sent!')
    } catch {
      setStatus('Something went wrong...')
    }
  }

  return (
    <>
      <div className='container'>
        <form class='form form--hidden' autocomplete='off'>
          <h1 class='form__title'>Comment</h1>
          <div class='form__message form__message--error'></div>
          <div class='form__input-group'>
            <TextField
              className='form__input-textBox'
              label='Text'
              value={text}
              oninput={(e: InputEvent) =>
                setText((e.target as HTMLInputElement).value)
              }
              outlined
            />
          </div>
          <Button onClick={submit}>post</Button>
          <div className='form-remainder'>
            <p>
              <a href='../app/signup'>Forgot to sign-up?</a>
            </p>
          </div>
          <div id='form-error'>
            {status && (
              <ul>
                <li class='status'>{status}</li>
              </ul>
            )}
          </div>
        </form>
      </div>
    </>
  )
}

export default PostComment
