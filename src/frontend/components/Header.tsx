import {h} from 'preact'
import {Link} from 'preact-router/match'

// @ts-ignore
import logo from 'url:../images/logo.png'
import '../styles/Header.scss'

export default () => (
  <header>
    <Link className='logo' href='/app/'>
      <img src={logo} alt='logo'></img>
    </Link>
    <nav>
      <Link
        href='/app/'
        className='header-top__navbar-item'
        activeClassName='header-top__navbar-item--active'
      >
        Home
      </Link>
      <Link
        href='/app/signup'
        className='header-top__navbar-item'
        activeClassName='header-top__navbar-item--active'
      >
        Sign-up
      </Link>
      <Link
        href='/app/login'
        className='header-top__navbar-item'
        activeClassName='header-top__navbar-item--active'
      >
        Login
      </Link>
      <Link
        href='/app/unifeed'
        className='header-top__navbar-item'
        activeClassName='header-top__navbar-item--active'
      >
        UniFeed
      </Link>
      <Link
        href='/app/parties'
        className='header-top__navbar-item'
        activeClassName='header-top__navbar-item--active'
      >
        Manage parties
      </Link>
    </nav>
  </header>
)
