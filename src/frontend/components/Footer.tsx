import {h, Fragment} from 'preact'
import Icon from 'preact-material-components/Icon'
import 'preact-material-components/Icon/style.css'

import '../styles/Footer.scss'

export default () => (
  <>
    <footer>
      <div className='footer-license'>
        Licensed under{' '}
        <a
          target='_blank'
          rel='noopener noreferrer'
          href='https://opensource.org/licenses/MIT'
        >
          MIT
        </a>
      </div>
      <div className='icons'>
        <a href='https://gitlab.com/losamtcifiiku/10-pi-hacks-2021'>
          <Icon>code</Icon>
        </a>
        <a href='https://devpost.com/software/partyverse'>
          <Icon>bookmark</Icon>
        </a>
      </div>
      <div className='footer-copyright'>
        © 2021 losamtcifiiku team. All rights reversed.
      </div>
    </footer>
  </>
)
