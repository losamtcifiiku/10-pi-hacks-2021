import {h, Component} from 'preact'
import Event from './Event'
import {dislikeEvent, getEvent, likeEvent} from '../../common/Api'
import {Static} from '@sinclair/typebox'
import {EventPOD} from '../../common/Event'

type Props = {
  id: number
}
type State = {event?: Static<typeof EventPOD>}

export class FullEvent extends Component<Props, State> {
  state: State = {}

  async componentWillMount() {
    this.setState({
      event: await getEvent(this.props.id),
    })
  }

  async onLike() {
    this.setState({
      event: await likeEvent(this.props.id),
    })
  }

  async onDislike() {
    this.setState({
      event: await dislikeEvent(this.props.id),
    })
  }

  render() {
    return (
      this.state.event && (
        <Event
          full
          event={this.state.event}
          onLike={this.onLike.bind(this)}
          onDislike={this.onDislike.bind(this)}
        />
      )
    )
  }
}

export default FullEvent
