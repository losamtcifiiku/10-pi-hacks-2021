import {h, Component} from 'preact'
import {getEventComments, getUser} from '../../common/Api'
import {Comment} from './Comment'

type Props = {id: number}
type State = {comments?: {author: string; text: string}[]}

export class Comments extends Component<Props, State> {
  async componentWillMount() {
    this.setState({
      comments: await Promise.all(
        (
          await getEventComments(this.props.id)
        ).map(async ({text, authorId}) => {
          return {text, author: (await getUser(authorId!)).name}
        })
      ),
    })
  }

  render() {
    return (
      this.state.comments && (
        <ul>
          {this.state.comments.map(comment => (
            <li>
              <Comment {...comment} />
            </li>
          ))}
        </ul>
      )
    )
  }
}

export default Comments
