import {h, Component} from 'preact'
import {signup} from '../../common/Api'
import TextField from 'preact-material-components/TextField'
import 'preact-material-components/TextField/style.css'
import {Button} from '../components/Button'

import '../styles/Form.scss'

type Validated<T> = {
  value: T
  message?: string
}

type State = {
  name: Validated<string>
  email: Validated<string>
  password: Validated<string>
  passwordConfirm: Validated<string>
  statusText?: string
}

export class SignUp extends Component<{}, State> {
  state: State = {
    name: {value: ''},
    email: {value: ''},
    password: {value: ''},
    passwordConfirm: {value: ''},
  }

  componentDidMount() {
    this.setName('')
    this.setEmail('')
    this.setPassword('')
    this.setPasswordConfirm('')
  }

  get messages() {
    function pred(x?: string): x is string {
      return x !== undefined
    }

    return [
      this.state.name.message,
      this.state.email.message,
      this.state.password.message,
      this.state.passwordConfirm.message,
    ].filter(pred)
  }

  setName(value: string) {
    this.setState({
      name: {
        value,
        message: value.trim() === '' ? 'Name must not be empty' : undefined,
      },
    })
  }

  setEmail(value: string) {
    this.setState({
      email: {
        value: value.trim(),
        message:
          /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/.test(
            value
          )
            ? undefined
            : 'Invalid email',
      },
    })
  }

  setPassword(value: string) {
    this.setState({
      password: {
        value,
        message:
          value.trim().length < 8 ? 'Minimal password length is 8' : undefined,
      },
    })
  }

  setPasswordConfirm(value: string) {
    this.setState({
      passwordConfirm: {
        value,
        message:
          value.trim() !== this.state.password.value
            ? 'Passwords do not match'
            : undefined,
      },
    })
  }

  async submit(e: Event) {
    e.preventDefault()

    const {
      name: {value: name},
      email: {value: email},
      password: {value: password},
    } = this.state

    if (this.messages.length === 0) {
      try {
        await signup(name, email, password)
        this.setState({statusText: 'Successfully signed up!'})
      } catch {
        this.setState({statusText: 'Something went wrong'})
      }
    }
  }

  render() {
    return (
      <div className='container'>
        <form class='form form--hidden' autocomplete='off'>
          <h1 class='form__title'>Create account</h1>
          <div class='form__message form__message--error'></div>
          <div class='form__input-group'>
            <TextField
              className='form__input-textBox'
              value={this.state.name.value}
              valid={this.state.name.message === undefined}
              onInput={(e: InputEvent) =>
                this.setName((e.target as HTMLInputElement).value)
              }
              label='Username'
              outlined
            />
            <TextField
              className='form__input-textBox'
              value={this.state.email.value}
              valid={this.state.email.message === undefined}
              onInput={(e: InputEvent) =>
                this.setEmail((e.target as HTMLInputElement).value)
              }
              label='Email Address'
              outlined
            />
          </div>
          <div class='form__input-group'>
            <TextField
              type='password'
              className='form__input-textBox'
              value={this.state.password.value}
              valid={this.state.password.message === undefined}
              onInput={(e: InputEvent) =>
                this.setPassword((e.target as HTMLInputElement).value)
              }
              label='Password'
              outlined
            />
            <TextField
              type='password'
              className='form__input-textBox'
              value={this.state.passwordConfirm.value}
              valid={this.state.passwordConfirm.message === undefined}
              onInput={(e: InputEvent) =>
                this.setPasswordConfirm((e.target as HTMLInputElement).value)
              }
              label='Confirm Password'
              outlined
            />
          </div>
          <Button onClick={this.submit.bind(this)}>register</Button>
          <div className='form-remainder'>
            <p>
              <a href='../app/login'>Already have an account? Login.</a>
            </p>
          </div>
          <ul id='form-error'>
            {this.state.statusText && (
              <li class='status'>{this.state.statusText}</li>
            )}
            {this.messages.map(message => (
              <li>{message}</li>
            ))}
          </ul>
        </form>
      </div>
    )
  }
}

export default SignUp
