import {Props} from '../../components/HeroSection'
// @ts-ignore
import statistics2 from 'url:../../images/statistics-2.svg'
// @ts-ignore
import site1 from 'url:../../images/first_site-1.svg'
// @ts-ignore
import top3_last from 'url:../../images/top_document-last-3.svg'

export const homeLayoutObjOne: Props = {
  lightBg: true,
  lightText: false,
  lightTextDesc: false,
  topLine: 'Be trendy!',
  headLine: 'Have you ever forgotten to come to a friend`s party?',
  description:
    'With our website, you can always set yourself a reminder, and fortunately, this problem will never bother you again!',
  buttonLabel: 'Start now',
  isLeftSided: true,
  src: site1,
  alt: 'Card',
}

export const homeLayoutObjTwo: Props = {
  lightBg: false,
  lightText: true,
  lightTextDesc: true,
  topLine: 'Incentive system',
  headLine: 'Earn reputation',
  description:
    'Now you have the motivation to run the best events on the planet earth! Build your reputation and brag about it to your friends.',
  buttonLabel: 'Learn more...',
  src: statistics2,
  alt: 'Stats',
}

export const homeLayoutObjAfterThree: Props = {
  lightBg: false,
  lightText: true,
  lightTextDesc: true,
  topLine: 'Make your way to the top!',
  headLine: 'Are you good enough?',
  description:
    'Become the top 1 at your university, and then compete against the rest. Let`s see how much of a "party king" you are.',
  buttonLabel: 'Get started',
  isLeftSided: true,
  src: top3_last,
  alt: 'Top7',
}
