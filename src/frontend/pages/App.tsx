import {h, FunctionComponent} from 'preact'
import Router, {Route} from 'preact-router'
import Header from '../components/Header'
import Footer from '../components/Footer'
import SignIn from './SignIn'
import SignUp from './SignUp'
import UniFeed from './UniFeed'
import ManageParties from './ManageParties'
import Home from './Home'
import ScrollUp from '../components/ScrollUp'
import FullEvent from '../components/FullEvent'

import '../styles/App.scss'

export const App: FunctionComponent = () => (
  <div id='app'>
    <Header />
    <main>
      <Router>
        <Route path='/app/' component={Home} />
        <Route path='/app/login' component={SignIn} />
        <Route path='/app/signup' component={SignUp} />
        <Route path='/app/unifeed' component={UniFeed} />
        <Route path='/app/parties' component={ManageParties} />
        <Route path='/app/events/:id' component={FullEvent} />
      </Router>
    </main>
    <Footer />
    <ScrollUp />
  </div>
)

export default App
