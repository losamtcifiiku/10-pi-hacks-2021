import {h, Fragment, Component} from 'preact'
import {Button} from '../components/Button'
import TextField from 'preact-material-components/TextField'
import 'preact-material-components/TextField/style.css'
import {request} from '../../common/Api'
import '../styles/Form.scss'

import '../styles/ManageParties.scss'
import {EventPOD} from '../../common/Event'
import {Static} from '@sinclair/typebox'
import {DeepPartial} from 'typeorm'

type Validated<T> = {
  value: T
  message?: string
}

type State = {
  title: Validated<string>
  date: Validated<string>
  place: Validated<string>
  university: Validated<string>
  description: Validated<string>
  statusText?: string
}

export class ManageParties extends Component<{}, State> {
  state: State = {
    title: {value: ''},
    date: {value: ''},
    place: {value: ''},
    university: {value: ''},
    description: {value: ''},
  }

  componentDidMount() {
    this.setTitle('')
    this.setDate('')
    this.setPlace('')
    this.setUniversity('')
    this.setDescription('')
  }

  get messages() {
    function pred(x?: string): x is string {
      return x !== undefined
    }

    return [
      this.state.title.message,
      this.state.date.message,
      this.state.place.message,
      this.state.university.message,
      this.state.description.message,
    ].filter(pred)
  }

  setTitle(value: string) {
    this.setState({
      title: {
        value: value,
        message:
          value.trim() === '' ? 'Name field must not be empty' : undefined,
      },
    })
  }

  setDate(value: string) {
    this.setState({
      date: {
        value: value,
        message:
          value.trim() === '' ? 'Date field must not be empty' : undefined,
      },
    })
  }

  setPlace(value: string) {
    this.setState({
      place: {
        value: value,
        message:
          value.trim() === '' ? 'Place field must not be empty' : undefined,
      },
    })
  }

  setUniversity(value: string) {
    this.setState({
      university: {
        value: value,
        message:
          value.trim() === ''
            ? 'University field must not be empty'
            : undefined,
      },
    })
  }

  setDescription(value: string) {
    this.setState({
      description: {
        value: value,
        message:
          value.trim() === ''
            ? 'Description field must not be empty'
            : undefined,
      },
    })
  }

  async submit(e: Event) {
    e.preventDefault()

    if (this.messages.length === 0) {
      const {
        title: {value: title},
        date: {value: date},
        place: {value: place},
        university: {value: university},
        description: {value: description},
      } = this.state

      const event: DeepPartial<Static<typeof EventPOD>> = {
        title,
        date,
        place,
        university,
        description,
      }

      await request('POST', '/api/events', event)

      this.setState({statusText: 'Congrats with new event!'})
    }
  }

  render() {
    return (
      <>
        <div className='container'>
          <form class='form form--hidden' autocomplete='off'>
            <h1 class='form__title'>Organize party</h1>
            <div class='form__message form__message--error'></div>
            <div class='form__input-group'>
              <TextField
                className='form__input-textBox'
                value={this.state.title.value}
                valid={this.state.title.message === undefined}
                onInput={(e: InputEvent) =>
                  this.setTitle((e.target as HTMLInputElement).value)
                }
                label='Name'
                outlined
              />
              <TextField
                className='form__input-textBox'
                value={this.state.date.value}
                valid={this.state.date.message === undefined}
                onInput={(e: InputEvent) =>
                  this.setDate((e.target as HTMLInputElement).value)
                }
                label='Date'
                outlined
              />
            </div>
            <div class='form__input-group'>
              <TextField
                className='form__input-textBox'
                value={this.state.place.value}
                valid={this.state.place.message === undefined}
                onInput={(e: InputEvent) =>
                  this.setPlace((e.target as HTMLInputElement).value)
                }
                label='Place'
                outlined
              />
              <TextField
                className='form__input-textBox'
                value={this.state.university.value}
                valid={this.state.university.message === undefined}
                onInput={(e: InputEvent) =>
                  this.setUniversity((e.target as HTMLInputElement).value)
                }
                label='University'
                outlined
              />
            </div>
            <div class='form__input-group'>
              <TextField
                className='form__input-textBox'
                value={this.state.description.value}
                valid={this.state.description.message === undefined}
                onInput={(e: InputEvent) =>
                  this.setDescription((e.target as HTMLInputElement).value)
                }
                label='Description'
                outlined
              />
            </div>
            <Button onClick={this.submit.bind(this)}>submit</Button>
            <div className='form-remainder'>
              <p>
                <a href='../app/login'>Make sure you are authorized!</a>
              </p>
            </div>
            <div id='form-error'>
              <ul>
                {this.state.statusText && (
                  <li class='status'>{this.state.statusText}</li>
                )}
                {this.messages.map(message => (
                  <li>{message}</li>
                ))}
              </ul>
            </div>
          </form>
        </div>
      </>
    )
  }
}

export default ManageParties
