import {Static} from '@sinclair/typebox'
import {h, Component} from 'preact'
import {Link} from 'preact-router'
import {getEvents, request} from '../../common/Api'
import {EventPOD} from '../../common/Event'
import Event from '../components/Event'
import '../styles/UniFeed.scss'

type State = {
  events: Static<typeof EventPOD>[]
}

export class UniFeed extends Component<{}, State> {
  state: State = {events: []}

  componentWillMount() {
    getEvents().then(events => this.setState({events}))
  }

  async like(i: number) {
    const events = this.state.events.slice()
    events[i].likes = (await request(
      'POST',
      `/api/events/${events[i].id}/like`
    )) as number
    this.setState({events})
  }

  async dislike(i: number) {
    const events = this.state.events.slice()
    events[i].likes = (await request(
      'POST',
      `/api/events/${events[i].id}/dislike`
    )) as number
    this.setState({events})
  }

  render() {
    return (
      <div class='container'>
        <h1 className='title'>All the events:</h1>
        <div>
          {this.state.events.map((event, i) => (
            <Link className='post' href={`/app/events/${event.id}`}>
              <Event
                event={event}
                onLike={this.like.bind(this, i)}
                onDislike={this.dislike.bind(this, i)}
              />
            </Link>
          ))}
        </div>
      </div>
    )
  }
}

export default UniFeed
