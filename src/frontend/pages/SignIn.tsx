import {h, Fragment, FunctionComponent} from 'preact'
import TextField from 'preact-material-components/esm/TextField'
import 'preact-material-components/TextField/style.css'
import {useState} from 'preact/hooks'
import {login} from '../../common/Api'
import {Button} from '../components/Button'

import '../styles/Form.scss'

export const SignIn: FunctionComponent = () => {
  const [name, setName] = useState('')
  const [password, setPassword] = useState('')
  const [status, setStatus] = useState('')

  async function submit(e: Event) {
    e.preventDefault()
    try {
      await login(name, password)
      setStatus('Successfully logged in!')
    } catch {
      setStatus('Something went wrong....')
    }
  }

  return (
    <>
      <div className='container'>
        <form class='form form--hidden' autocomplete='off'>
          <h1 class='form__title'>Log in</h1>
          <div class='form__message form__message--error'></div>
          <div class='form__input-group'>
            <TextField
              id='filled-basic'
              className='form__input-textBox'
              label='Username'
              value={name}
              oninput={(e: InputEvent) =>
                setName((e.target as HTMLInputElement).value)
              }
              outlined
            />
          </div>
          <div class='form__input-group'>
            <TextField
              type='password'
              className='form__input-textBox'
              label='Password'
              value={password}
              oninput={(e: InputEvent) =>
                setPassword((e.target as HTMLInputElement).value)
              }
              outlined
            />
          </div>
          <Button onClick={submit}>log in</Button>
          <div className='form-remainder'>
            <p>
              <a href='../app/signup'>Haven't got one? Sign-up.</a>
            </p>
          </div>
          <div id='form-error'>
            {status && (
              <ul>
                <li class='status'>{status}</li>
              </ul>
            )}
          </div>
        </form>
      </div>
    </>
  )
}

export default SignIn
