import {Type} from '@sinclair/typebox'
import {type} from 'os'
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm'
import {Comment} from './Comment'
import {User} from './User'

@Entity()
export class Event {
  @PrimaryGeneratedColumn()
  id?: number

  @Column()
  title?: string

  @Column()
  date?: string

  @Column()
  place?: string

  @Column()
  university?: string

  @Column()
  description?: string

  @Column()
  likes?: number

  @ManyToOne(() => User, user => user.events)
  author?: Promise<User>

  @OneToMany(() => Comment, comment => comment.event)
  comments?: Promise<Comment[]>
}

export const EventPOD = Type.Object({
  id: Type.Optional(Type.Number()),
  author: Type.Optional(Type.Number()),
  title: Type.String(),
  date: Type.String(),
  place: Type.String(),
  university: Type.String(),
  description: Type.String(),
  likes: Type.Optional(Type.Number()),
})
