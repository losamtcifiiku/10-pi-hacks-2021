import {Type} from '@sinclair/typebox'
import {Entity, PrimaryGeneratedColumn, Column, OneToMany} from 'typeorm'
import {Comment} from './Comment'
import {Event} from './Event'

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id?: number

  @Column({nullable: false})
  name?: string

  @Column({nullable: false})
  email?: string

  @Column({nullable: false})
  password?: string

  @OneToMany(() => Event, event => event.author)
  events?: Promise<Event[]>

  @OneToMany(() => Comment, comment => comment.author)
  comments?: Promise<Comment[]>
}

export const UserPOD = Type.Object({
  id: Type.Optional(Type.Number()),
  name: Type.String(),
  email: Type.String(),
  password: Type.Optional(Type.String()),
})
