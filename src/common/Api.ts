import {Static} from '@sinclair/typebox'
import {SHA256} from 'crypto-js'
import {CommentPOD} from './Comment'
import {EventPOD} from './Event'
import {UserPOD} from './User'

export type ApiResponse<T> =
  | {statusCode: undefined; value: T}
  | {statusCode: number; error: string; message: string}

export type AuthToken = {
  user: number
  password: string
}

export type AuthedRequest<T> = AuthToken & {value: T}

export async function request(
  method: string,
  url: string,
  req?: unknown
): Promise<unknown> {
  const response = await fetch(url, {
    method,
    headers:
      req === undefined ? undefined : {'Content-Type': 'application/json'},
    body: JSON.stringify(req),
    credentials: 'same-origin',
    mode: 'same-origin',
  })

  const json = (await response.json()) as ApiResponse<unknown>

  if (json.statusCode !== undefined) {
    throw `${json.error}: ${json.message}`
  }

  return json.value
}

export async function isLoggedIn() {
  try {
    await request('GET', '/api/self')
    return true
  } catch {
    return false
  }
}

export async function login(name: string, password: string) {
  return (await request('POST', '/api/login', {
    name,
    password: SHA256(password).toString(),
  })) as void
}

export async function signup(name: string, email: string, password: string) {
  return (await request('POST', '/api/users', {
    name,
    email,
    password: SHA256(password).toString(),
  })) as void
}

export async function getEvents() {
  return (await request('GET', '/api/events')) as Static<typeof EventPOD>[]
}

export async function getEvent(eventId: number) {
  return (await request('GET', `/api/events/${eventId}`)) as Static<
    typeof EventPOD
  >
}

export async function likeEvent(eventId: number) {
  return (await request('POST', `/api/events/${eventId}/like`)) as Static<
    typeof EventPOD
  >
}

export async function dislikeEvent(eventId: number) {
  return (await request('POST', `/api/events/${eventId}/dislike`)) as Static<
    typeof EventPOD
  >
}

export async function getEventComments(eventId: number) {
  return (await request('GET', `/api/events/${eventId}/comments`)) as Static<
    typeof CommentPOD
  >[]
}

export async function postComment(eventId: number, text: string) {
  return (await request('POST', '/api/comments', {
    eventId,
    text,
  })) as Static<typeof CommentPOD>
}

export async function getUser(userId: number) {
  return (await request('GET', `/api/users/${userId}`)) as Static<
    typeof UserPOD
  >
}
