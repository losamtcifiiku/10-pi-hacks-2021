import {Entity, PrimaryGeneratedColumn, Column, ManyToOne} from 'typeorm'
import {User} from './User'
import {Event} from './Event'
import {Type} from '@sinclair/typebox'

@Entity()
export class Comment {
  @PrimaryGeneratedColumn()
  id?: number

  @Column({nullable: false})
  text?: string

  @ManyToOne(() => User, user => user.comments)
  author?: Promise<User>

  @ManyToOne(() => Event, event => event.comments)
  event?: Promise<Event>
}

export const CommentPOD = Type.Object({
  text: Type.String(),
  authorId: Type.Optional(Type.Number()),
  eventId: Type.Number(),
})
