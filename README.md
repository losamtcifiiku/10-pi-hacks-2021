## partyverse

## Elevator pitch

Parties are just an _integral part_ of our **fun student life**. And now with the **partyverse**, you will be _much more_ comfortable organizing them, commenting on them, and inviting your friends or new people!

## About the project

## Inspiration

Student life is just full of all kinds of events. Vespers help a lot in communicating with people, finding new cool friends, and just as a rest. So to help students better organize and plan various parties, look through the ones that will be held at your university, or comment on the ones you have been to, we created this web application.

## What it does

_Our site consists of several categories:_

- Registration page.
- Home page - where we talk about ourselves, our goals, and a little guide on how to use us.
- UniFeed page where you can see all the upcoming parties that will be held at your university.
- Your Events and Create Events are pages where you can create your event (also invite friends) and look through previous events you have attended and like/dislike and comment on them.
  **Overall, the site just allows you to conveniently manage your upcoming parties.**

## How we built it

We took [typescript](https://www.typescriptlang.org/) as the main programming language - it is much easier to work with a static programming language in a team because only using a pre-prepared declaration of types you can easily implement the logic of the application without going too deep into what your other partner (for example on the backend) is doing.
For the frontend framework we use [preact](https://preactjs.com/) - an entirely rewritten by now (used to be a fork) [React](https://reactjs.org/), which provides mostly all modern API of React, but also greatly speeds it up (it weighs 3 KB actually) without much loss in usability.
As a web application bundler we use [Parcel](https://parceljs.org/) - it is blazing fast, and you do not need to config everything at all.
Also, we are hosting our project on [Heroku](https://www.heroku.com/) using [Nix OS](https://nixos.org/) image for a server (all the needed flakes are provided on GitLab too!!!). For testing the dev branch release we prefer [stormkit](https://www.stormkit.io/) over Heroku since it is easier to set up and faster and more flexible, but sadly paid.
For package management we use [yarn](https://yarnpkg.com/) - it is very space-efficient because files inside node_modules are linked from single content-addressable storage in zip archive. It is also quite strict and secure - it creates non-flat node_modules, so the code has no access to arbitrary packages. Yarn Plug’n’Play feature helps you to manage modules in a much more superior way.
[Babel](https://babeljs.io/) operates as a transcompiler. It is free and open-source and mainly used to convert ECMAScript 2015+ (ES6+) code into a backward-compatible version of JavaScript that can be run by older JavaScript engines. Babel is a popular tool for using the newest features of the JavaScript programming language.
And [pug.js](https://pugjs.org/) helped us as a templating tool that made our HTML writing life a lot easier.
Code quality monitoring is an integral process in any kind of development, even during a hackathon. That is why we always have and bring the config files for [stylelint](https://stylelint.io/) linter && [prettier](https://prettier.io/) formatter.
The heart of our project. The backend. An efficient server implies a lower cost of the infrastructure, better responsiveness under load, and happy users. How can you efficiently handle the resources of your server, knowing that you are serving the highest number of requests possible, without sacrificing security validations and handy development?
That is why we enter [Fastify](https://www.fastify.io/). Fastify is a web framework highly focused on providing the best developer experience with the least overhead and a powerful plugin architecture. It is inspired by [Hapi](https://hapi.dev/) and [Express](https://expressjs.com/) and as far as we know, it is one of the fastest web frameworks in the Wild West.
As a database, we use [PostgreSQL](https://www.postgresql.org/). It is a powerful, open-source object-relational database system with over 30 years of active development that has earned it a strong reputation for reliability, feature robustness, and performance. The best thing about it, that it is quite easier to use it on Heroku than other tools.

## Challenges we ran into

We had _never_ had to work with React or preact before, but using these tools is essential for modern web development, so we had to figure it out completely from scratch, and to eliminate one of the main disadvantages of React - namely its size and speed, we immediately started working with preact, although to understand it without experience in React was quite difficult for us.
After that, another problem arose - **how the hell do you put it all together**? Here the parcel came to the rescue, fortunately, although the preact is small for documentation about the building part, we managed to assemble everything and deploy it.
Fastify, though a good library, has a very small ecosystem, unfortunately, and some handy libraries under [Node](https://nodejs.org/) we lacked.
In general, it was also our first experience with databases.
PostgreSQL, although pretty powerful, is very complex and difficult to learn. We probably should have picked a different tool. True, in that case, free hosting is still in question.

## Accomplishments that we’re proud of

We managed to implement a **fully** working prototype, which is, by the way, incredibly swift and light!

## What we learned

During the development process, we realized how boring, long, and tedious it is to set up a building system (_but it is!_).
We doped out how to work with react components in conjunction with typescript, as well as how to deploy and configure the server (have never worked on a Nix OS system in the cloud, and it was a lot of fun! I still think immutable patterns do not fit well in our case, and it is harder to have everything work).
We have also improved our skills in SQL and teamwork on fairly big projects.

## What’s next for partyverse

First, we have to work on the adaptation, of course. Our application at the moment, although it works on phones, it is not as perfect and convenient as we would like it to be. Perhaps an even better option would be a mobile app, which would use the same API as the website, but would be much faster for phones and more pumped up with some interesting stuff.
An extended party search would also be an absolutely good extension. Such as if they could be sorted by time, more specific location, number of people.
Also, an incredibly important element of the portal should be a function of event visibility. For example, something like private or public, or an age restriction.
Presentation (AKA demo-video || &pitching)
Hello, this is the losamtcifiiku team, and we are happy to present to you our partyverse project. So first, what is partyverse? Partyverse tackles many problems college students face. Our website focuses more on the fun and social side. Time management is a common problem among college students. With partyverse, you can keep track of all the parties that are being hosted on your campus and make sure you attend them. With all the stress from exams and classes, parties are a great way to relax and relieve all your stress. So now I will just be going through the features of our website.
First, we have our home page where you can find out more information about our page and easily get signed up. It shows some reasons to use our product and lets users know how we work. Next, we have the sign-up and login page where you can create an account or login into your existing account to view the parties on your campus. Then we have a UniFeed page where you can see all the upcoming parties and their reviews, likes, and so forth. Lastly, we have a manage parties page where you can create a new event and see all the previous parties you attended.
Thanks for your attention, I hope you enjoyed our demo!
